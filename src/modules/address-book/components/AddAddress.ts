import Address from '../types/Address'

export const AddAddress = {
  name: 'AddAddress',
  methods: {
    addAddress (address: Address) {
      address['custom_attributes'] = [
        { 'attribute_code': 'block', 'value': address.block },
        { 'attribute_code': 'floor', 'value': address.floor },
        { 'attribute_code': 'avenue', 'value': address.avenue }
      ];
      return new Promise((resolve) => {
        this.$store.dispatch('addressBook/addAddress', address)
        resolve()
      })
    },
    updateAddress (currentAddressId, address: Address) {
      address['custom_attributes'] = [
        { 'attribute_code': 'block', 'value': address.block },
        { 'attribute_code': 'floor', 'value': address.floor },
        { 'attribute_code': 'avenue', 'value': address.avenue }
      ];
      console.log(address);
      return new Promise((resolve) => {
        this.$store.dispatch('addressBook/updateAddress', { 'currentAddressId': currentAddressId, 'newAddress': address })
        resolve()
      })
    }
  }
}
