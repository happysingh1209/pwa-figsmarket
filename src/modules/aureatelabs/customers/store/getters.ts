import CustomerState from '../types/CustomerState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<CustomerState, any> = {
  getTokenList: (state) => state.tokens,
  hasItems: (state) => state.tokens && state.tokens.length > 0
}
