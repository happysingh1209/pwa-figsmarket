import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { NotifyMeModuleStore } from './store/index';

export const NotifyMeModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('notifyme', NotifyMeModuleStore)
};
